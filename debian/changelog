biniou (1.2.2-4) UNRELEASED; urgency=medium

  * Use camlp-streams in ocamldoc

 -- Stéphane Glondu <glondu@debian.org>  Fri, 14 Jun 2024 08:29:47 +0200

biniou (1.2.2-3) unstable; urgency=medium

  * Team upload
  * Use ocaml_dune DH buildsystem
  * Bump Standards-Version to 4.6.2

 -- Stéphane Glondu <glondu@debian.org>  Mon, 07 Aug 2023 08:30:13 +0200

biniou (1.2.2-2) unstable; urgency=medium

  * Team upload
  * Fix build with recent dune

 -- Stéphane Glondu <glondu@debian.org>  Sat, 15 Jul 2023 07:17:09 +0200

biniou (1.2.2-1) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * New upstream release

  [ Debian Janitor ]
  * Update watch file format version to 4
  * Bump debhelper from old 12 to 13
  * Set upstream metadata fields
  * Update standards version to 4.6.1

 -- Stéphane Glondu <glondu@debian.org>  Sat, 21 Jan 2023 08:23:50 +0100

biniou (1.2.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update debian/watch
  * Bump Standards-Version to 4.4.1
  * Add Rules-Requires-Root: no
  * Build-depend on debhelper-compat and remove debian/compat

 -- Stéphane Glondu <glondu@debian.org>  Thu, 19 Dec 2019 11:17:35 +0100

biniou (1.2.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release (Closes: #898272)
  * Update Homepage and debian/watch
  * Bump debhelper compat level to 12
  * Bump Standards-Version to 4.4.0
  * Remove Hendrik from Uploaders
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Mon, 12 Aug 2019 07:09:19 +0200

biniou (1.0.12-2) unstable; urgency=medium

  * Team upload
  * Fix FTBFS on bytecode architectures

 -- Stéphane Glondu <glondu@debian.org>  Wed, 03 Aug 2016 22:19:16 +0200

biniou (1.0.12-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update Homepage and debian/watch
  * Update Vcs-*
  * Bump Standards-Version to 3.9.8

 -- Stéphane Glondu <glondu@debian.org>  Wed, 03 Aug 2016 13:29:53 +0200

biniou (1.0.9-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 3.9.5

 -- Stéphane Glondu <glondu@debian.org>  Fri, 31 Jan 2014 11:13:23 +0100

biniou (1.0.8-1) unstable; urgency=low

  * Team upload
  * New upstream release
    - remove patch fix-bi_stream-for-32-bit-OCaml, since upstream changes
      now deal explicitly with 32-bit platforms

 -- Stéphane Glondu <glondu@debian.org>  Thu, 11 Jul 2013 11:36:49 +0200

biniou (1.0.6-1) unstable; urgency=low

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

  [ Hendrik Tews ]
  * update watch
  * add myself as uploader
  * bump debhelper compat level and standards version
  * update Vcs, dependencies and package description
  * update copyright
  * rename and adapt debian-changes patch
  * add patch fix-bi-stream-32
  * update man page
  * install api docs in api/html

 -- Hendrik Tews <hendrik@askra.de>  Thu, 13 Jun 2013 14:01:31 +0200

biniou (1.0.0-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Bump Standards-Version to 3.9.2

 -- Stéphane Glondu <glondu@debian.org>  Mon, 11 Jul 2011 00:02:27 +0200

biniou (0.9.1-1) unstable; urgency=low

  * Initial release. (Closes: #605672)

 -- Sylvain Le Gall <gildor@debian.org>  Sun, 05 Dec 2010 00:07:39 +0100
